<?php
namespace Jokuf\Flash;

/**
 * This class acts as enum and holds messages types
 *
 * @category Messages
 * @package  Messages
 * @author   Radoslav Yordanov <jokuf2010@gmail.com>
 * @license  MIT
 */
interface Type
{
    const ERROR   = "error";
    const SUCCESS = "success";
    const INFO    = "info";
    const WARNING = "warning";
    const DANGER  = "danger";
}
