<?php
namespace Jokuf\Flash;
use Jokuf\Storage\SessionStorage;

/**
*
*/
abstract class Message implements MessageTypeProtocol
{
    /** @var array storage for message attributes */
    protected $message = array();
    
    /**
     * 
     * @param string $msg  message to be send
     * @param timestamp $time time when message is send
     */
    function __construct($msg, $time=null) {
        if(!$time) {
            $time = time();
        }

        $this->message['type'] = $this->type;
        $this->message['time'] = $time;
        $this->message['body'] = $msg;
    }
    /**
     * JSON Representation of message and all of its attributes
     * @return string JSON string
     */
    public function __toString(){
        return $this->toJSON();
    }

    /**
     * Is message body is empty
     * @return boolean Return if message body is empty string
     */
    public function isEmpty(){
        return empty($this->message['body']);
    }

    /**
     * JSON Representation of Object
     * @return string JSON representation
     */
    public function toJSON(){
        return json_encode($this->message);
    }
    /**
     * Return array of message attributes
     * @return array message attributes[body, time, type]
     */
    public function dump(){
        return $this->message;
    }

    /**
     * Return Message Type[css class]
     * @return string Return message type[success, error, warning, info, danger]
     */
    public function getType(){
        return $this->type;
    }

    public function __destruct()
    {
        if(!isset($_SESSION[Notifier::SESSION_NAME]) or !is_array($_SESSION[Notifier::SESSION_NAME])){
            $_SESSION[Notifier::SESSION_NAME] = array();
        }

        $_SESION[Notifier::SESSION_NAME][] = $this->message;
    }

    /**
     * Return Message Body
     * @return string Return message body
     */
    public function getBody(){
        return $this->message['body'];
    }
    /**
     * Get time when message was submitted
     * @return timestamp timestamp of submitted message
     */
    public function getTime() {
        return $this->message['time'];
    }
    /**
     * Message class Initialization class
     * @param  string $message Message to be send
     * @return Message         Retur new instance of called class
     */
    public static function send($message){
        $cls = get_called_class();

        return new $cls($message);
    }
}
?>
