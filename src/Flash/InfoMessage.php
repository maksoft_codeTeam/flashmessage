<?php
namespace Jokuf\Flash;


/**
 * This class acts as enum and holds framework types
 *
 * Page-Level DocBlock
 *
 * @category Messages
 * @package  InfoMessage
 * @author   Radoslav Yordanov <jokuf2010@gmail.com>
 * @license  http://jokuf.com/ asd
 */
class InfoMessage extends Message {

    /**
   * The date when the deposit was opened.
   *
   * @var Type
   */
    protected $type = Type::INFO;
}

 ?>
