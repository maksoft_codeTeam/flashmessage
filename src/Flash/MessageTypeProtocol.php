<?php
namespace Jokuf\Flash;


interface MessageTypeProtocol {

    /**
     * Return Message Class, Time and Body as JSON
     *
     */
    public function toJSON();

    /**
     * Return Message Class,Time and Body as JSON
     *
     * @return String JSON representation of class attributes
     */
    public function __toString();

    /**
     *
     * @return array Return message attributes.
     */
    public function dump();
}
