<?php
namespace Jokuf\Flash;


/**
 *
 * @implements \Iterator
 * @implements \Countable
 * @implements \ArrayAccess
 * @implements \Serializable
 */
class Notifier implements \Countable
{
    /** DEFAULT SESSION KEY TO STORE OBJ*/
    const SESSION_NAME="flashNotifications";
    /** @var \SplObjectStorage messages storage */
    private $messages=array();

    public function __construct() {
        $sessionStorage = $_SESSION[self::SESSION_NAME];
        if(isset($sessionStorage) and !empty($sessionStorage)){
            $this->messages = $sessionStorage;
            var_dump($this->messages);
        }
    }

    public function getMessages() {
        return $this->messages;
    }

    public function count(){
        return count($this->messages);
    }

    public function add(Message $msg){
        $id = spl_object_hash($msg);
        $this->messages[$id] = $msg;
        return $this;
    }

    public function delete(Message $msg){
        $id = spl_object_hash($msg);
        if(array_key_exists($id, $this->messages)){
            unset($this->messages[$id]);
            return true;
        }
        return false;
    }

    public function deleteAll() {
        $this->messages = array();
    }

    public function save()
    {
        $_SESSION[self::SESSION_NAME] = $this->messages;
    }

    function __destruct() {
        $_SESSION[self::SESSION_NAME] = $this->messages;
    }

    public static function init()
    {
        $tmp = new Notifier();
        return $tmp;
    }
}
