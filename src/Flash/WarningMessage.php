<?php
namespace Jokuf\Flash;


/**
 * This class acts as enum and holds framework types
 *
 * Page-Level DocBlock
 *
 * @category Messages
 * @package  WarningMessage
 * @author   Radoslav Yordanov <jokuf2010@gmail.com>
 * @license  http://jokuf.com/ asd
 */
class WarningMessage extends Message {
    /**
   * The date when the deposit was opened.
   *
   * @var Type
   */
    protected $type = Type::WARNING;
}

?>
