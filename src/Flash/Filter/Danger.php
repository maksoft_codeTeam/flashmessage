<?php
namespace Jokuf\Flash\Filter;


use Jokuf\Flash\DangerMessage;

/**
*
*/
class Danger extends Base
{
    public function accept()
    {
        $thing = $this->getInnerIterator()->current();
        return $thing instanceof DangerMessage;
    }
}

 ?>
