<?php
namespace Jokuf\Flash\Filter;


use Jokuf\Flash\ErrorMessage;

/**
*
*/
class Error extends Base
{
    public function accept()
    {
        $thing = $this->getInnerIterator()->current();
        return $thing instanceof ErrorMessage;
    }
}

 ?>
