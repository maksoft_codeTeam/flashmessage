<?php
namespace Jokuf\Flash\Filter;


use Jokuf\Flash\WarningMessage;

/**
*
*/
class Warning extends Base
{
    public function accept()
    {
        $thing = $this->getInnerIterator()->current();
        return $thing instanceof WarningMessage;
    }
}

 ?>
