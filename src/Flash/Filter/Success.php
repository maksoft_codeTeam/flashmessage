<?php
namespace Jokuf\Flash\Filter;


use Jokuf\Flash\SuccessMessage;

/**
*
*/
class Success extends Base
{
    public function accept()
    {
        $thing = $this->getInnerIterator()->current();
        return $thing instanceof SuccessMessage;
    }
}

 ?>
