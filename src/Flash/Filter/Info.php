<?php
namespace Jokuf\Flash\Filter;


use Jokuf\Flash\InfoMessage;

/**
*
*/
class Info extends Base
{
    public function accept()
    {
        $thing = $this->getInnerIterator()->current();
        return $thing instanceof InfoMessage;
    }
}

 ?>
