<?php
namespace Jokuf\Flash;
/**
 * This class acts as enum and holds framework types
 *
 * @category Messages
 * @package  Messages
 * @author   Radoslav Yordanov <jokuf2010@gmail.com>
 * @license  http://jokuf.com/ asd
 */
interface Framework
{
    const BOOTSTRAP = 1;
    const FOUNDATION = 2;
    const MATERIALIZE = 3;
}
